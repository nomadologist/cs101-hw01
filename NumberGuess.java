/*
 * Author: Jack Booth
 * Date: 9/7/2016
 * 
 * Generates a random number between 0 and 9 and then tells whether a user guess is correct, too high, or too low
 */

import java.util.Scanner;

public class NumberGuess
{
	public static void main(String[] args)
	{
//		Ask for input
		System.out.println("Guess a number between 0 and 9");
		
//		Generate random number
		int number = (int)(Math.random()*10);
		
//		Collect input
		Scanner scan = new Scanner(System.in);
		int guess = scan.nextInt();
		
//		Compare number and return result
		if(guess == number)
			System.out.println("Correct number!");
		if(guess > number)
			System.out.println("Guess is too high, the correct number was " + number);
		if(guess < number)
			System.out.println("Guess is too low, the correct number was " + number);
	}
}
