/*
 * Author: Jack Booth
 * Date: 9/7/2016
 * 
 * Collects input to play rock paper scissors against a random computer.
 */
import java.util.Scanner;

public class RockPaperScissors
{
	public static void main(String[] args)
	{
//		Collect user input.
		System.out.println("Choose your weapon: rock(0), paper(1), or scissors(2)");
		Scanner scan = new Scanner(System.in);
		int input = scan.nextInt();
		
//		Generate rock, paper, or scissors for computer.
		int weapon = (int)(Math.random()*3);
		
//		Makes sure input is valid
		if(input>2 || input<0)
			System.out.println("Invalid input, please enter a number between 0 and 2");
		
//		Compares input to computer generation and decides a winner.
		else if(input == weapon)
			System.out.println("Draw");
		else if(weapon == 0)
		{
			if(input == 1)
				System.out.println("You win! Paper beats rock.");
			if(input == 2)
				System.out.println("You lose. Rock beats scissors.");
		}
		else if(weapon == 1)
		{
			if(input == 0)
				System.out.println("You lose. Paper beats rock.");
			if(input == 2)
				System.out.println("You win! Scissors beats paper.");
		}
		else if(weapon == 2)
		{
			if(input == 0)
				System.out.println("You win! Rock beats scissors");
			if(input == 1)
				System.out.println("You lose. Scissors beats paper");
		}
	}
}
