/*
 * Author: Jack Booth
 * Date: 9/7/2016
 * 
 * Generates a random password with uppercase and lowercase letters as well as numbers.
 */
public class PasswordGenerator
{
	public static void main(String[] args)
	{
//		Declaring two alphabet strings.
		String lowerAlphabet = "abcdefghijklmnopqrstuvwxyz";
		String upperAlphabet = "ABCDEFGHIKLMNOPQRSTUVWXYZ";
		
//		Generating numbers and letters
		int r = (int) (Math.random() * 26);
		char char1 = lowerAlphabet.charAt(r);
		int char2 = (int) (Math.random() * 10);
		int r2 = (int) (Math.random() * 26);
		char char3 = upperAlphabet.charAt(r2);
		int r3 = (int) (Math.random() * 26);
		char char4 = lowerAlphabet.charAt(r3);
		int char5 = (int) (Math.random()*10);
		int char6 = (int) (Math.random()*10);
		
//		Printing out generated password.
		System.out.print("Generated Password: ");
		System.out.print(char1);
		System.out.print(char2);
		System.out.print(char3);
		System.out.print(char4);
		System.out.print(char5);
		System.out.print(char6);
	}
}
